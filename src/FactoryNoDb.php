<?php

namespace Karls\ApiCore;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

abstract class FactoryNoDb
{
    use withFaker;

    protected const MODEL = null;
    protected Collection $state;
    protected int $count = 1;
    protected bool $submissiveState = true;

    public function __construct()
    {
        $this->state = new Collection();
        $this->setUpFaker();
    }

    public function submissiveState(): self
    {
        $this->submissiveState = true;
        return $this;
    }

    public function dominantState(): self
    {
        $this->submissiveState = false;
        return $this;
    }

    public function create(array $state = []): Collection|ModelNoDb
    {
        $this->state($state);
        $instances = new Collection();

        for ($i = 0; $i < $this->count; $i++) {
            $rawDefinition = collect($this->definition())->merge($this->state);
            $definition = $rawDefinition->map(fn($d) => is_callable($d) ? $d($i) : $d);
            $instances->push(new (static::model())($definition));
        }

        return $instances->count() > 1 ? $instances : $instances->first();
    }

    public function state(array $state): self
    {
        $this->state = $this->submissiveState
            ? $this->state->merge(collect($state))
            : collect($state)->merge($this->state);

        return $this;
    }

    abstract public function definition(): array;

    protected static function model(): string
    {
        return static::MODEL ?? Str::beforeLast(static::class, 'Factory');
    }

    public function count(int $i): self
    {
        $this->count = $i;
        return $this;
    }
}
