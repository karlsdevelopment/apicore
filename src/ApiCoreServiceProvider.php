<?php

namespace Karls\ApiCore;

use Illuminate\Support\ServiceProvider;

class ApiCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/apicoreconfig.php' => config_path('apicoreconfig.php'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/apicoreconfig.php', 'apicoreconfig'
        );
        parent::register();
    }
}