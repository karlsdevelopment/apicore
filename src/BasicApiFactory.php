<?php

namespace Karls\ApiCore;

use Illuminate\Http\Client\Factory;

abstract class BasicApiFactory
{
    public function __construct()
    {
        $this->fake();
    }

    abstract public function fake(?array $callback = null): Factory;
}
