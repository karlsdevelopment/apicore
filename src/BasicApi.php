<?php

namespace Karls\ApiCore;

use Exception;
use Illuminate\Http\Client\Factory;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Str;
use Karls\Core\Exceptions\CoreException;
use Nette\MemberAccessException;

abstract class BasicApi extends Http
{
    protected const V_PREFIX = '';
    protected const FACTORY = null;

    abstract static function host(): string;

    public static function refreshFake($callback = []): Factory
    {
        $routes = [];
        foreach ($callback as $route => $response) {
            $routes[self::url($route)] = $response;
            unset($callback[$route]);
        }
        return parent::fake($routes);
    }

    /**
     * @throws CoreException
     * @deprecated
     */
    public function __call(string $method, array $args): mixed
    {
        if (in_array($method, ['put', 'post', 'get'])) {
            return self::$method($args);
        }

        throw new MemberAccessException('Method ' . $method . ' not exists');
    }

    /**
     * @throws CoreException
     */
    public static function post(string $url, array $data = [], array $files = [], array $options = []): ?array
    {
        $r = parent::acceptJson();
        self::attachFiles($r, $files);
        return static::request('post', $url, $data, $r, options: $options);
    }

    /**
     * @throws CoreException
     */
    public static function put(string $url, array $data = [], array $options = []): array
    {
        return static::request('put', $url, $data, options: $options);
    }

    /**
     * @throws CoreException
     */
    public static function get(string $url, array|string|null $query = [], array $options = []): array
    {
        return static::request('get', $url, $query, options: $options);
    }

    /**
     * @throws CoreException
     * @throws Exception
     */
    protected static function request(
        string $method,
        string $endpoint,
        array $params = [],
        ?PendingRequest $request = null,
        array $options = []
    ) {
        $r = $request ?? parent::acceptJson();
        [$request, $params] = static::prepareRequest($r, $params);

        try {
            $response = $request->$method(self::url($endpoint, options: $options), $params);
            $json = $response->json();
            $response->throw();

        } catch (Exception $e) {
            $details = $json['details'] ?? [];

            $exceptionType = Config::get('apicoreconfig.exceptionType');
            if ($exceptionType === Exception::class) {
                throw new Exception($e->getMessage(), static::errorCodes('service'), $e);
            }

            throw new $exceptionType(static::errorCodes('service'),
                details: $details,
                message: $e->getMessage(),
                previous: $e
            );
        }
        return $json;
    }

    protected static function prepareRequest(PendingRequest $r, array $params): array
    {
        return [$r, $params];
    }

    protected static function attachFiles(PendingRequest $request, array $files)
    {
        foreach ($files ?? [] as $name => $path) {
            $file = fopen($path, 'r');
            $request = $request->attach($name, $file);
        }
    }

    public static function url(string $endpoint = '', array $replace = [], array $options = []): string
    {
        $endpoint = Str::start(Str::swap($replace, $endpoint), '/');

        return static::host() . ($options['prefix'] ?? static::V_PREFIX) . $endpoint;
    }

    protected static function errorCodes(string $name): int
    {
        return match ($name) {
            'service' => 1,
            'default' => 0,
        };
    }

    public static function asMultipart(): PendingRequest
    {
        return parent::asMultipart();
    }

    public static function __callStatic($method, $args)
    {
        return app(static::factoryName())->$method(...$args);
    }

    protected static function factoryName(): string
    {
        return static::FACTORY ?? (static::class . 'Factory');
    }

    public static function fake($callback = []): Factory
    {
        return app(static::factoryName())->fake($callback);
    }
}
