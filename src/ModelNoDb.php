<?php

namespace Karls\ApiCore;

use Illuminate\Contracts\Support\Arrayable;

abstract class ModelNoDb
{
    protected const FACTORY = null;

    public function __construct(null|Arrayable|array $data = null)
    {
        return empty($data) ? null : $this->update($data);
    }

    public static function factory(int $i = 1): FactoryNoDb
    {
        $factoryName = static::FACTORY ?? static::class . 'Factory';
        return (new $factoryName())->count($i);
    }

    public function toArray(): array
    {
        return (array)$this;
    }

    public function update(array|Arrayable $data): static
    {
        foreach ($data as $k => $v) {
            property_exists($this, $k) && $this->$k = $v;
        }
        return $this;
    }
}
